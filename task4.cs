public class SquareEquation
{
    public static double[] Solve(double a,
                                double b,
                                double c)
    {

        double eps = 1e-5;
        if (double.IsNaN(a) || double.IsInfinity(a) || double.IsNaN(b) || double.IsInfinity(b) || double.IsNaN(c) || double.IsInfinity(c))
        {
            throw new System.ArgumentException();
        }
        if ( Math.Abs(a) < eps)
        {
            throw new System.ArgumentException();
        }

        double d = b * b - 4 * a * c;
        double x1, x2;
        if(Math.Abs(d) < eps){
            double sqrtD = (double)System.Math.Sqrt(d);
            x1 = (-b) / (2 * a);
            double[] ans = new double[2];
            ans[0] = x1;
            ans[1] = x1;
            return ans;
        }
        if (d <  -eps)
        {
            return new double[0];
        }
        if( Math.Abs(b) < eps && Math.Abs(c) < eps){
            double[] ans = new double[2];
            ans[0] = 0;
            ans[1] = 0;
            return ans;
    
          
        }
        else{
            double sqrtD = System.Math.Sqrt(d);
            x1 = (sqrtD) / (2 * a);
            x2 = c / x1;
            double[] ans = new double[2];
            ans[0] = x1;
            ans[1] = x2;
            return ans;
        }
    }

    public static void Main()
    {
        double a, b, c;
        Console.WriteLine("Введите коэффициент a:");
        a = Double.Parse(Console.ReadLine(), System.Globalization.CultureInfo.InvariantCulture);
        Console.WriteLine("Введите коэффициент b:");
        b = Double.Parse(Console.ReadLine(), System.Globalization.CultureInfo.InvariantCulture);
        Console.WriteLine("Введите коэффициент c:");
        c = Double.Parse(Console.ReadLine(), System.Globalization.CultureInfo.InvariantCulture);

        try
        {
        
            double[] answer = new double[2];
            answer = SquareEquation.Solve(a, b, c);
            if(answer.Length == 0){
                Console.WriteLine($"Корней нет");
            }
            Console.WriteLine($"Уравнение:{a}x^2 + {b}x + {c} = 0");
            Console.WriteLine($"Корень x1 = {answer[0]} невязка {a*answer[0]*answer[0] +b*answer[0]+c} ");
            Console.WriteLine($"Корень x2 = {answer[1]} невязка {a*answer[1]*answer[1]+b*answer[1]+c} ");
        }
        catch (System.ArgumentException)
        {
            Console.WriteLine("Неверные параметры квадратного уравнения");
        }
    }
}